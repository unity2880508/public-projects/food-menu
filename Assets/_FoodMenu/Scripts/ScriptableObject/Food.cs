using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScriptableObjectIdAttribute : PropertyAttribute { }

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ScriptableObjectIdAttribute))]
public class ScriptableObjectIdDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        GUI.enabled = false;
        if (string.IsNullOrEmpty(property.stringValue)) {
            property.stringValue = Guid.NewGuid().ToString();
        }
        EditorGUI.PropertyField(position, property, label, true);
        GUI.enabled = true;
    }
}
#endif

[CreateAssetMenu(fileName = "Food", menuName = "ScriptableObject/New Food")]
public class Food : ScriptableObject
{
    [ScriptableObjectId]
    public string id;
    public Sprite icon;
    public string title;
    public string description;
}

[CreateAssetMenu(fileName = "FoodMenu", menuName = "ScriptableObject/Food Menu")]
public class Menu : ScriptableObject 
{
    public List<Food> foods;
}
