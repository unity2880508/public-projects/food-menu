using TMPro;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    [SerializeField] private Menu _menu;
    [SerializeField] private GameObject _foodRowPrefab;
    [SerializeField] private Transform _parentTransform;

    // Start is called before the first frame update
    void Start()
    {
        // Với mỗi giá trị food thuộc danh sách món ăn lưu trong menu
        // sinh ra 1 gameobject hiển thị giá trị đó (_foodRowPrefab)
        foreach(var food in _menu.foods) 
        {
            _foodRowPrefab.GetComponent<FoodRowView>().IconImage.sprite = food.icon;
            _foodRowPrefab.GetComponent<FoodRowView>().TitleText.text = food.title;
            _foodRowPrefab.GetComponent<FoodRowView>().Food = food;
            Instantiate(_foodRowPrefab, _parentTransform);
        }
    }
}
