using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InformationPopup : MonoBehaviour
{
    [SerializeField] private TMP_Text _contentText;
    [SerializeField] private Button _okButton;
    public static InformationPopup Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        
        Show("");
        _okButton.onClick.AddListener(
            () => gameObject.SetActive(false));
    }

    public void Show(string content)
    {
        _contentText.text = content;
        gameObject.SetActive(content != "");
    }
}
